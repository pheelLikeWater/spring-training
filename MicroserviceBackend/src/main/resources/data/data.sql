INSERT INTO Person(name, description, price) values ('Apfel', 'Ein roter Apfel.', '0.32');
INSERT INTO Person(name, description, price) values ('Birne', 'Eine unreife Birne.', '0.23');
INSERT INTO Person(name, description, price) values ('Pflaume', 'Groß', '0.42');
INSERT INTO Person(name, description, price) values ('Banane', 'Grün', '0.12');
INSERT INTO Person(name, description, price) values ('Orange', 'Super für Saft geeignet.', '1.32');
INSERT INTO Person(name, description, price) values ('Blutorange', 'Eine schöne rote Farbe.', '1.12');