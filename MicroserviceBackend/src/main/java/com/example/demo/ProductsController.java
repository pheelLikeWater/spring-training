package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductsController {

	@Autowired
    ProductsRepository productRepository;
	
	@RequestMapping(value = "/products", method=RequestMethod.GET, produces={"application/json; charset=UTF-8"})
    public List<Product> products() {
		return productRepository.findAll();
    }
	
}
