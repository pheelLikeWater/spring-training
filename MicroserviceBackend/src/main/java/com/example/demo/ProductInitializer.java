//package com.example.demo;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.ApplicationArguments;
//import org.springframework.boot.ApplicationRunner;
//import org.springframework.stereotype.Component;
//
//@Component
//public class DataLoader implements ApplicationRunner {
//
//    private ProductRepository repository;
//
//    @Autowired
//    public DataLoader(ProductRepository repository) {
//        this.repository = repository;
//    }
//
//    public void run(ApplicationArguments args) {
//		repository.save(new Product(1, "Apfel", "Ein roter Apfel.", 0.12f));
//		repository.save(new Product(2, "Birne", "Eine unreife Birne.", 0.23f));
//		repository.save(new Product(3, "Pflaume", "Groß", 0.42f));
//		repository.save(new Product(4, "Banane", "Grün", 0.12f));
//		repository.save(new Product(5, "Orange", "Super für Saft geeignet.", 1.12f));
//		repository.save(new Product(6, "Blutorange", "Eine schöne rote Farbe.", 1.22f));
//    }
//}