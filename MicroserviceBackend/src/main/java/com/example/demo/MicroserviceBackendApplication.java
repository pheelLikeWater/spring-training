package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@EnableResourceServer
@SpringBootApplication
public class MicroserviceBackendApplication {

	private static final Logger log = LoggerFactory.getLogger(MicroserviceBackendApplication.class);
	
	public static void main(String[] args) {
		SpringApplication.run(MicroserviceBackendApplication.class, args);
	}
	
//	@Bean
//	public CommandLineRunner demo(ProductRepository repository) {
//		return (args) -> {
//			// save a couple of customers
//			repository.save(new Product(1, "Apfel"));
//			repository.save(new Product(2, "Birne"));
//			repository.save(new Product(3, "Pflaume"));
//			repository.save(new Product(4, "Banane"));
//			repository.save(new Product(5, "Orange"));
//			repository.save(new Product(6, "Blutorange"));
//
//			// fetch all customers
//			log.info("Customers found with findAll():");
//			log.info("-------------------------------");
//			for (Product product : repository.findAll()) {
//				log.info(product.toString());
//			}
//			log.info("");
//
//			// fetch an individual customer by ID
//			Product product = repository.findOne(1L);
//			log.info("Customer found with findOne(1L):");
//			log.info("--------------------------------");
//			log.info(product.toString());
//			log.info("");
//
//			// fetch customers by last name
//			log.info("Customer found with findByLastName('Bauer'):");
//			log.info("--------------------------------------------");
//			for (Product orange : repository.findByName("Orange")) {
//				log.info(orange.toString());
//			}
//			log.info("");
//		};
//	}
}
