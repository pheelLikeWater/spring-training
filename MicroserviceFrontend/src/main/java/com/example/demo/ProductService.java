package com.example.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ProductService {
	
	private static final Logger log = LoggerFactory.getLogger(MicroserviceFrontendApplication.class);
	
	Iterable<Product> getProducts() {
		RestTemplate restTemplate = new RestTemplate();
        Product[] products = restTemplate.getForObject("http://localhost:8080/products", Product[].class);
        log.info(products[0].toString());
        
        ArrayList<Product> result = new ArrayList();
        result.add(products[0]);
        result.add(products[1]);
        result.add(products[2]);
        result.add(products[3]);
        
        return result;
	}
	
}


//@Service
//public class ProductService {
//
//    //private RestTemplate template = new RestTemplate();
//    private final OAuth2RestTemplate template; 
//
//    @Autowired
//    public ProductService(OAuth2RestTemplate template) {
//        this.template = template;
//    }
//
////    @HystrixCommand(fallbackMethod = "fallbackProducts",
////            commandProperties = {  
////            		@HystrixProperty(name="execution.isolation.strategy", value="SEMAPHORE")
////            })
//    public Collection<Product> getAllProducts() {
//
//        ResponseEntity<Product[]> response = template.getForEntity(
//                "http://localhost:8080/products", Product[].class);
//
//        return Arrays.asList(response.getBody());
//    }
//
//    public Collection<Product> fallbackProducts() {
//        return Collections.emptyList();
//    }
//}