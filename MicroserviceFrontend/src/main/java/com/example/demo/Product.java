package com.example.demo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Product {	
	
	private  long id;
    private  String name;
    private  String description;
    private  float price;
    
    protected Product() {}
    
    public Product(long id, String name, String description, float price) {
    	this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    
    public String getDescription() {
        return description;
    }
    
    public float getPrice() {
        return price;
    }
    
    @Override
    public String toString() {
        return String.format(
        		"Product[name='%s' %d , description='%s', price='%f']",
                name, id, description, price);
    }
}
