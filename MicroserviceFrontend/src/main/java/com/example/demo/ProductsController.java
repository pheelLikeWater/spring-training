package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Controller
public class ProductsController {

    @Autowired
    ProductService productService; 

    @RequestMapping("/ui")
    @HystrixCommand(fallbackMethod = "reliable")
    String list(Model model){
        Iterable<Product> products = productService.getProducts(); 
        model.addAttribute("products", products);
        return "products";
    }
    
    public String reliable(Model model) {
        return "products";
      }
}